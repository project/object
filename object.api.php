<?php

// Create a new user:
$account = new drupalUser();
$account->name = 'test';
$account->mail = 'test@example.com';
$account->pass = 'pass';
$account->status = '1';
$account->save();

// Create a new page node:
$node = new drupalNode('page');
$node->title = 'Test Node';
$node->setField('body', 'Lorem Ipsum Dolor Sit Amet', 0, 'value');
$node->setField('body', 'plain_text', 0, 'format');
$node->save();

// Change the body of node 1:
$node = new drupalNode(1);
$node->setField('body', 'New Body Text', 0, 'value');
$node->save();
