<?php

/**
 * Class wrapper for users.
 */
class drupalUser extends drupalEntity {
  protected $entityType = 'user';
  protected $idKey = 'uid';
  private $edit = array(); // An edit array to track changes before saving.
  
  /**
   * Need a custom set function since we need to keep track of all values that 
   * were changed in an edit array so that we can pass them on user_save.
   */
  public function set($property, $value) {
    parent::set($property, $value);
    $this->edit[$property] = $value;
  }
  
  /**
   * Need a custom set function since we need to keep track of all values that 
   * were changed in an edit array so that we can pass them on user_save.
   * 
   * Not sure this handles all scenarios with fields but it is a start.
   */
  public function setField($field_name, $value, $id = NULL, $attribute = NULL) {
    parent::set_field($field_name, $value, $id, $attribute);
    $this->edit[$field_name] = $this->get_field($field_name);
  }
 
  public function loadByMail($mail) {
    $this->_entity = user_load_by_mail($mail);
  }
  
  public function loadByName($name) {
    $this->_entity = user_load_by_name($name);
  }
  
  public function access($string) {
    return user_access($string, $this->_entity);
  }
  
  public function hasPermission($string) {
    return $this->access($string);
  }
  
  /**
   * Check if the user has a role. Can pass either the role id or the role name.
   */
  public function hasRole($role) {
    if (!is_numeric($role)) {
      return in_array($role, array_keys($this->_entity->roles));
    }
    else {
      return in_array($role, $this->entity->roles);
    }
  }
  
  public function save($category = 'account') {
    // Need to do something here as it expects a save array.
    $result = user_save($this->_entity, $this->edit, $category);    
    if ($result) {
      $this->_entity = $result;
    }
    return (bool) $result; // Return if the save is succesful or not.
  }
  
  public function uri() {
    return user_uri($this->_entity);
  }
}