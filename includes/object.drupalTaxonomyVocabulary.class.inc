<?php

/**
 * Class wrapper for taxonomy vocabularies.
 */
class drupalTaxonomyVocabulary extends drupalEntity {
  protected $entityType = 'taxonomy_vocabulary';
  protected $idKey = 'vid';
  
}