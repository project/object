<?php

/**
 * Base class for entities.
 */
class drupalEntity {
  protected $entityType;
  protected $idKey;
  protected $_entity;
  
  public function __construct($eid = NULL) {
    if (!is_null($eid)) {
      $this->load($eid);
      // If it doesn't load, create it.
      if (!$this->_entity) {
        $this->create($eid);
      }
    }
  }
  
  /**
   * magic method to return non public properties
   *
   * @see     get
   * @param   mixed $property
   * @return  mixed
   */
  public function __get($property) {
    return $this->get($property);
  }

  /**
   * get specifed property
   *
   * @param mixed $property
   * @return mixed
   */
  public function get($property) {
    $value = null;

    if (is_object($this->_entity)) {
      if (isset($this->_entity, $property)) {
        return $this->_entity->{$property};
      } else {
        return null;
      }
    }
    return null;
  }
  
  public function getField($field_name, $id = NULL, $attribute = NULL) {
    $items = field_get_items($this->entityType, $this->_entity, $field_name, $this->_entity->language);
    if (is_numeric($id) && !is_null($attribute)) {
      return $items[$id][$attribute];
    }
    elseif (is_numeric($id)) {
      return $items[$id];
    }
    return $items;
  }

  /**
   * magic method to set non public properties
   *
   * @see    set
   * @param  mixed $property
   * @param  mixed $value
   * @return void
   */
  public function __set($property, $value) {
    $this->set($property, $value);
  }

  /**
   * set property to specified value
   *
   * @param mixed $property
   * @param mixed $value
   * @return void
   */
  public function set($property, $value) {
    if (!is_object($this->_entity)) {
      $this->_entity = new stdClass();
    }
    $this->_entity->{$property} = $value;
  }
  
  public function setField($field_name, $value, $id = NULL, $attribute = NULL) {
    if (!isset ($this->_entity->language)) {
      $this->_entity->language = LANGUAGE_NONE;
    }
    if (is_numeric($id) && !is_null($attribute)) {
      $this->_entity->{$field_name}[$this->_entity->language][$id][$attribute] = $value;
    }
    elseif (is_numeric($id)) {
      $this->_entity->{$field_name}[$this->_entity->language][$id] = $value;
    }
    else {
      $this->_entity->{$field_name}[$this->_entity->language] = $value;
    }
  }
  
  public function __isset($property) {
    // Not sure there is a way to do this with objects. property_exists can't detect properties accessible from __get.
    // return property_exists($this->_entity, $property);
    return array_key_exists($property, (array) $this->_entity);
  }
  
  public function __unset($property) {
    unset($this->_entity->{$property});
  }

  public function create($eid) {
    $this->{$this->idKey} = $eid;
  }
  
  public function load($eid) {
    $function = $this->entityType . '_load';
    $this->_entity = $function($eid);
  }
  
  public function save() {
    $function = $this->entityType . '_save';
    $this->_entity = $function($this->_entity);
  }
  
  public function delete() {
    if (isset($this->_entity->{$this->idKey})) {
      $function = $this->entityType . '_delete';
      return $function($this->_entity->{$this->idKey});
    }
  }
  
  /**
   * For debugging only!
   * @return type 
   */
  public function getEntity() {
    return $this->_entity;
  }
}