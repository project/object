<?php

/**
 * This is a standard ORM Model for interacting with a single row of a database table.
 */
class drupalModel {
  protected $table;
  protected $primary_key;
  protected $_data;
  
  public function __construct($id = '') {
    if ($id) {
      $this->load($id);
    }
  }

  /**
   * magic method to return non public properties
   *
   * @see     get
   * @param   mixed $property
   * @return  mixed
   */
  public function __get($property) {
    return $this->get($property);
  }

  /**
   * get specifed property
   *
   * @param mixed $property
   * @return mixed
   */
  public function get($property) {
    $value = null;

    if (is_array($this->_data)) {
      if (array_key_exists($property, $this->_data)) {
        return $this->_data[$property];
      } else {
        return null;
      }
    }
    return null;
  }

  /**
   * magic method to set non public properties
   *
   * @see    set
   * @param  mixed $property
   * @param  mixed $value
   * @return void
   */
  public function __set($property, $value) {
    $this->set($property, $value);
  }

  /**
   * set property to specified value
   *
   * @param mixed $property
   * @param mixed $value
   * @return void
   */
  public function set($property, $value) {
    $this->_data[$property] = $value;
  }
  
  public function __isset($property) {
    return array_key_exists($property, $this->_data);
  }
  
  public function __unset($property) {
    unset($this->_data[$property]);
  }
  
  public function __toString() {
    return serialize($this->_data);
  }
  
  /**
   * For debugging only.
   * 
   * @return array
   */
  public function getData() {
    return $this->_data;
  }

  public function create() {
    return drupal_write_record($this->table, $this->_data);
  }

  public function read($id) {
    $data = db_select($this->table, 't')
      ->fields('t')
      ->condition($this->primary_key, $id)
      ->execute()
      ->fetchAssoc();
    
    if (is_array($data)) {
      $this->_data = $data;
    }
    else {
      $this->_data = array();
    }
  }

  public function update() {
    return drupal_write_record($this->table, $this->_data, array($this->primary_key));
  }

  public function delete() {
    db_delete($this->table)
      ->condition($this->primary_key, $this->get($this->primary_key))
      ->execute();
  }
  
  public function exists() {
    $query = db_select($this->table, 't')
      ->fields('t')
      ->condition($this->primary_key, $this->get($this->primary_key))
      ->execute();

    return (bool) $query->fetchObject();    
  }
  
  public function load($id) {
    $this->read($id);
    // If the record doesn't exist, set the id.
    if (!$this->{$this->primary_key}) {
      $this->{$this->primary_key} = $id;
    }

  }
  
  public function save() {
    if ($this->exists()) {
      $this->update();
    }
    else {
      $this->create();
    }
  }
}
