<?php

/**
 * Class wrapper for nodes.
 */
class drupalNode extends drupalEntity {
  protected $entityType = 'node';
  protected $idKey = 'nid';
  
  public function __construct($param) {
    // If a nid is passed in, load the nid.
    if (is_numeric($param)) {
      $this->load($param);
      return;
    }
    // Check if the param is a node_type. If so, build a new node.
    $types = node_type_get_types();
    if (in_array($param, array_keys($types))) {
      $this->create($param);
      return;
    }
    // What here?
  }
  
  public function create($type) {
    global $user;
    $this->_entity = new stdClass();
    $this->_entity->type = $type;
    $this->_entity->name = $user->name; // uid will be set during node_submit.
    $this->_entity->language = LANGUAGE_NONE; //Should we be detecting this somewhere?
  }
  
  /**
   * Save is different for nodes so override default save for entities.
   */
  public function save() {
    if ($this->_entity = node_submit($this->_entity)) {
      node_save($this->_entity);
    }
  }
  
  public function access($op, $account = NULL) {
    return node_access($op, $this->_entity, $account);
  }
  
  public function lastChanged() {
    return node_last_changed($this->_entity->nid);
  }
  
  public function lastViewed() {
    return node_last_viewed($this->_entity->nid);
  }
  
  public function validate($form, &$form_state) {
    return node_validate($this->_entity, $form, $form_state);
  }
}