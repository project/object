<?php

/**
 * Class wrapper for Taxonomy Terms.
 */
class drupalTaxonomyTerm extends drupalEntity {
  protected $entityType = 'taxonomy_term';
  protected $idKey = 'tid';
  
}